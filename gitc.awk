#/usr/bin/env gawk -f
BEGIN {
  FS="\n"
  RS="\0"
}
{
  commit = gensub(/^commit\s*(.*)\s*.*$/, "\\1", "g", $1)
  author = gensub(/^Author:\s*(.*)\s*<.*>$/, "\\1", "g", $2)
  gsub(/^\s*/, "", author) # remove leading whitespace
  gsub(/\s*$/, "", author) # remove trailing whitespace
  date = gensub(/^Date:\s*(.*)$/, "\\1", "g", $3)
  num_files_changed = int(gensub(/^.*\s+([0-9]+) files? changed.*$/, "\\1", "g", $0))
  num_insertions = int(gensub(/^.*\s+([0-9]+) insertions?\(\+\).*$/, "\\1", "g", $0))
  num_deletions = int(gensub(/^.*\s+([0-9]+) deletions?\(\-\).*$/, "\\1", "g", $0))
  valid = num_insertions >= min_insertions
  if (valid || count_invalid) {
    commits[author]++
    files[author] += num_files_changed
    insertions[author] += num_insertions
    deletions[author] += num_deletions
  }
  if (!valid && warn_invalid) {
    printf "Invalid commit %s by %s.\n", commit, author
    # if (!count_invalid) {
    #   printf "Invalid commit %s wasn't counted.\n", commit
    # }
  }
}
END {
  for (author in commits) {
    author_commits = commits[author]
    if (author_commits < min_commits_per_author) {
      exit 1
    }
    total += author_commits
     printf "%s: %d commits (%d insertions(+), %d deletions(-))\n",
       author, commits[author], insertions[author], deletions[author]
  }
   printf "Total number of commits: %d\n", total
  if (total < min_commits) {
    exit 1
  }
}
